package sofka.business;

import java.util.List;

import sofka.entities.Carril;
import sofka.entities.Carro;

public interface CarroService {
	public Carro crearCarro(Carro carro);
	public List<Carro> listarCarros();
	public Carro asignarCarril(Carril carril, Carro carro);
}
