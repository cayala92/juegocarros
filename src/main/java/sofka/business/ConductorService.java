package sofka.business;

import java.util.List;

import sofka.entities.Carro;
import sofka.entities.Conductor;

public interface ConductorService {	
	public Conductor crearConductor(Conductor conductor);
	public List<Conductor> listarConductores();
	public Conductor asignarCarro(Carro carro, Conductor conductor);
}
