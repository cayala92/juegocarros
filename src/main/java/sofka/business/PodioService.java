package sofka.business;

import java.util.List;

import sofka.entities.Jugador;
import sofka.entities.Pista;
import sofka.entities.Podio;

public interface PodioService {
	public Podio crearPodio(Podio podio);
	//asignar id de juego
	public Podio asignarPista(Pista  pista, Podio podio);
	public Podio asignarResultados(Podio podio, Jugador primerLugar, Jugador segundoLugar, Jugador tercerLugar);
	public List<Podio> listarResultados();
	public Podio buscarResultadosPorId(Long idPodio);
}
