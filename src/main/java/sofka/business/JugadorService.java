package sofka.business;

import java.util.List;

import sofka.entities.Conductor;
import sofka.entities.Jugador;

public interface JugadorService {
	public Jugador crearJugador(Jugador jugador);
	public List<Jugador> listarJugadores();
	public Jugador asignarConductor(Conductor conductor, Jugador jugador);
	public Jugador actualizarVecesPrimerLugar(Jugador jugador);
	public Jugador actualizarVecesSegundoLugar(Jugador jugador);
	public Jugador actualizarVecesTercerLugar(Jugador jugador);	
}
