package sofka.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sofka.Repository.CarrilRepository;
import sofka.business.CarrilService;
import sofka.entities.Carril;

@Service
public class CarrilServiceImpl implements CarrilService{

	@Autowired
	CarrilRepository carrilRepository;
	
	@Override
	public Carril crearCarril(Carril carril) {
		return carrilRepository.save(carril);
	}

	@Override
	public List<Carril> listarCarriles() {
		return carrilRepository.findAll();
	}
	
}
