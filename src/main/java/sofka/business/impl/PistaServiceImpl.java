package sofka.business.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sofka.Repository.PistaRepository;
import sofka.business.PistaService;
import sofka.entities.Pista;

@Service
public class PistaServiceImpl implements PistaService {

	@Autowired
	PistaRepository pistaRepository;
	
	@Override
	public Pista crearPista(Pista pista) {
		return pistaRepository.save(pista);
	}

	@Override
	public List<Pista> listarPistas() {
		return pistaRepository.findAll();
	}

}
