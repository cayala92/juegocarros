package sofka.business;

import java.util.List;

import sofka.entities.Pista;

public interface PistaService {
	public Pista crearPista(Pista pista);
	public List<Pista> listarPistas();
}
