package sofka.business;

import java.util.List;

import sofka.entities.Carril;

public interface CarrilService {
	public Carril crearCarril(Carril carril);
	public List<Carril> listarCarriles();
}
