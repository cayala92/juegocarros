package sofka.business;

public interface JuegoService {
	public void configurarJuego(int numeroJugadores);
	public void seleccionarPista();
	public void crearPista();
	public void asignarCarril();
	public void crearCarril();
	public void seleccionarCarro();
	public void crearCarro();
	public void seleccionarConductor();
	public void crearConductor();
	public void crearPodio();
	public void guardarResultadosPodio();
	public void seleccionarPodio();
	
	
}
