package sofka.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sofka.entities.Carro;

@Repository
public interface CarroRepository extends JpaRepository<Carro, Long> {
	
}
