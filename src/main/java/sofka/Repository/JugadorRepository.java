package sofka.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sofka.entities.Jugador;

@Repository
public interface JugadorRepository extends JpaRepository<Jugador,Long> {
	
}
