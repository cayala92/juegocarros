package sofka.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sofka.entities.Pista;

@Repository
public interface PistaRepository extends JpaRepository<Pista,Long> {
	
}
