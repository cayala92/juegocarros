package sofka.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sofka.entities.Podio;

@Repository
public interface PodioRepository extends JpaRepository<Podio,Long> {
	
}
