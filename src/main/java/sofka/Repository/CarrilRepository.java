package sofka.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import sofka.entities.Carril;

@Repository
public interface CarrilRepository extends JpaRepository<Carril,Long> {
	
}
