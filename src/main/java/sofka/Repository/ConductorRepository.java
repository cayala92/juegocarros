package sofka.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import sofka.entities.Conductor;

public interface ConductorRepository extends JpaRepository<Conductor,Long> {
	
}
