package sofka;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import sofka.entities.Carril;
import sofka.entities.Carro;
import sofka.entities.Conductor;
import sofka.entities.Jugador;
import sofka.entities.Pista;
import sofka.entities.Podio;

@SpringBootApplication
public class JuegoCarrosApplication {

	
	
	
	public static void main(String[] args) {
		
		List<Carro> carros = new ArrayList<Carro>();
		List<Carril> carriles = new ArrayList<Carril>();
		List<Jugador> jugadores = new ArrayList<Jugador>();
		List<Conductor> conductores = new ArrayList<Conductor>();
		Pista pista = new Pista();
		Podio podio= new Podio();
		SpringApplication.run(JuegoCarrosApplication.class, args);
		
		Scanner leer = new Scanner(System.in);
        boolean salir = false;
        boolean estaConfigurado = false;
        int opcion; //Guardaremos la opcion del usuario
        int numeroJugadores = 1;
        while (!salir) {
 
            System.out.println("1. ConfigurarJuego \n 2. Iniciar Juego \n 3. salir");
 
            try {
 
                System.out.println("Escribe una de las opciones");
                opcion = leer.nextInt();
 
                switch (opcion) {
                    case 1:
                    	
                    	if(estaConfigurado) {
                    		break;
                    	}else {
	                    	System.out.println("Configurando Juego...");
	                        System.out.println("Numero de Jugadores...");
	                        numeroJugadores = leer.nextInt();
	                        for (int i = 0; i < numeroJugadores; i ++) {
	                        	jugadores.add(crearJugador());
	                        }
	                        
	                        
	                        for (Jugador jugador:jugadores) {
	                        	conductores.add(crearConductor(jugador));
	                        }
	                        
	                        for (Conductor conductor:conductores) {
	                        	carros.add(crearCarro(conductor));
	                        }
	                        
	                        for (Carro carro:carros) {
	                        	carriles.add(crearCarril(carro));
	                        }
	                        
	                        //Crear pista y agregar carriles
	                        pista = crearPista();
	                        for (Carril carril:carriles) {
	                        	carril.setPista(pista);
	                        }
                    	}
                    	estaConfigurado= true;
                        System.out.println("Juego configurado, por favor iniciar...");
                        break;
                        

                    case 2:
                    	if (!estaConfigurado) {
                    		System.out.println("Configure el Juego primero...");
                    	}else {
                    		System.out.println("Iniciando Juego...");           
                    		iniciarJuego(pista, jugadores, conductores, carros, carriles);
                    	}
                    	break;	
                        
                    case 3:
                        salir = true;
                        break;
                    default:
                        System.out.println("Solo n�meros entre 1 y 3");
                }
            } catch (InputMismatchException e) {
                System.out.println("Debes insertar un n�mero");
                leer.next();
            }
        }
 
    }
	
	
	public static Pista crearPista() {
		Scanner leer = new Scanner(System.in);
		System.out.println("Escriba el nombre de la pista");
		String nombrePista = leer.nextLine();
		System.out.println("Escriba la distancia de la pista");
		Long distanciaPista  = leer.nextLong();
		
		Pista pista = new Pista();
		pista.setNombre(nombrePista);
		pista.setDistanciaLimite(distanciaPista);
		return pista;
	}
	
	public static Carril crearCarril(Carro carro) {
		Scanner leer = new Scanner(System.in);
		System.out.println("Carro "+ carro.getNombre() + " Conductor: " + carro.getConductor().getApodo() + " jugador: " +carro.getConductor().getJugador().getApellidos() + " " + carro.getConductor().getJugador().getNombres());
		System.out.println("Escriba el nombre del carril");
		String nombreCarril = leer.nextLine();
		
		Carril carril = new Carril();
		carril.setNombre(nombreCarril);
		carril.setCarro(carro);
		return carril;
	}
	
	public static Carro crearCarro(Conductor conductor) {
		Scanner leer = new Scanner(System.in);
		System.out.println("Conductor: " + conductor.getApodo() + " jugador: " +conductor.getJugador().getApellidos() + " " + conductor.getJugador().getNombres());
		System.out.println("Escriba el nombre del carro");
		String nombre = leer.nextLine();
		System.out.println("Escriba el color del carro");
		String color = leer.nextLine();
		Long distancia = 0L;
		System.out.println("Escriba el modelo del carro");
		String modelo = leer.nextLine();
		System.out.println("Escriba la placa del carro");
		String placa = leer.nextLine();		

		Carro carro = new Carro();
		carro.setNombre(nombre);
		carro.setColor(color);
		carro.setDistancia(distancia);
		carro.setModelo(modelo);
		carro.setPlaca(placa);
		carro.setConductor(conductor);
		return carro;
	}
	
	public static Conductor crearConductor(Jugador jugador) {
		Scanner leer = new Scanner(System.in);
		System.out.println("Escriba el apodo del conductor para el jugador " + jugador.getApellidos()  + " " + jugador.getNombres());
		String apodo = leer.nextLine();
		Conductor conductor = new Conductor();
		conductor.setApodo(apodo);
		conductor.setJugador(jugador);
		return conductor;
	}
	
	public static Jugador crearJugador() {
		Scanner leer = new Scanner(System.in);
		System.out.println("Escriba el nombre del jugador");
		String nombre = leer.nextLine();
		System.out.println("Escriba el apellido del jugador");
		String apellido = leer.nextLine();
		System.out.println("Escriba la edad del jugador");
		int edad = Integer.parseInt(leer.nextLine());
		System.out.println("Escriba el tipo de documento del jugador");
		String tipoDocumento = leer.nextLine();
		System.out.println("Escriba el numero de documento del jugador");
		String numeroDocumento = leer.nextLine();
		
		
		Jugador jugador = new Jugador();
		jugador.setNombres(nombre);
		jugador.setApellidos(apellido);
		jugador.setEdad(edad);
		jugador.setTipoDocumento(tipoDocumento);
		jugador.setNumDocumento(numeroDocumento);
		jugador.setVecesPrimerLugar(0L);
		jugador.setVecesSegundoLugar(0L);
		jugador.setVecesTercerLugar(0L);
		return jugador;
	}
	
	public Podio crearPodio(Jugador primerLugar, Jugador segundoLugar, Jugador tercerLugar) {
		Podio podio = new Podio();
		podio.setPrimerLugar(primerLugar);
		podio.setSegundoLugar(segundoLugar);
		podio.setTercerLugar(tercerLugar);
		return podio;
	}
	
	public static int lanzarDado() {
		return (int)(Math.random()*6 + 1);
	}
	public static Carro avanzarCarro(Carro carro) {
		carro.setDistancia(carro.getDistancia() + lanzarDado()*100);
		
		return carro;
	}
	
	public static void iniciarJuego(Pista pista,List<Jugador> jugadores,List<Conductor> conductores,List<Carro> carros, List<Carril> carriles) {
		
		boolean primerLugar = false, segundoLugar=false, tercerLugar=false, juegoConcluido = false;
		Podio podio = new Podio();
		while (!juegoConcluido) {
			for (int i = 0; i < carriles.size(); i++){
				
				System.out.println("Turno del jugador: "+ (i+1) + jugadores.get(i).getApellidos() +" " +  jugadores.get(i).getNombres());
				Long distanciaActual = carriles.get(i).getCarro().getDistancia();
				carriles.get(i).setCarro(avanzarCarro(carriles.get(i).getCarro()));
				Long nuevaDistancia = carriles.get(i).getCarro().getDistancia();
				System.out.println("Distancia al iniciar el turno: "+ distanciaActual + "\n Desplazamiento: " + (nuevaDistancia-distanciaActual)+ "\n nuevaDistancia: " + nuevaDistancia);
				if (nuevaDistancia >= carriles.get(i).getPista().getDistanciaLimite()*1000){
					carriles.get(i).getCarro().setDistancia(carriles.get(i).getPista().getDistanciaLimite());
					if (primerLugar ==false) {
						primerLugar=true;
						podio.setPrimerLugar(carriles.get(i).getCarro().getConductor().getJugador());
						carriles.get(i).getCarro().getConductor().getJugador().setVecesPrimerLugar(carriles.get(i).getCarro().getConductor().getJugador().getVecesPrimerLugar()+1);
					}else if(segundoLugar==false){
						segundoLugar=true;
						podio.setSegundoLugar(carriles.get(i).getCarro().getConductor().getJugador());
						carriles.get(i).getCarro().getConductor().getJugador().setVecesSegundoLugar(carriles.get(i).getCarro().getConductor().getJugador().getVecesSegundoLugar()+1);
					}else if(tercerLugar==false) {
						tercerLugar=true;
						podio.setTercerLugar(carriles.get(i).getCarro().getConductor().getJugador());

						carriles.get(i).getCarro().getConductor().getJugador().setVecesTercerLugar(carriles.get(i).getCarro().getConductor().getJugador().getVecesTercerLugar()+1);

						juegoConcluido=true;
						System.out.println("Primer lugar: "+ podio.getPrimerLugar().getApellidos() +" " +podio.getPrimerLugar().getNombres());
						System.out.println("veces primer lugar: "+ podio.getPrimerLugar().getVecesPrimerLugar());
						System.out.println("veces segundo lugar: "+ podio.getPrimerLugar().getVecesSegundoLugar());
						System.out.println("veces tercer lugar: "+ podio.getPrimerLugar().getVecesTercerLugar());
						System.out.println("Segundo lugar: "+ podio.getSegundoLugar().getApellidos() +" " +podio.getSegundoLugar().getNombres());
						System.out.println("veces primer lugar: "+ podio.getSegundoLugar().getVecesPrimerLugar());
						System.out.println("veces segundo lugar: "+ podio.getSegundoLugar().getVecesSegundoLugar());
						System.out.println("veces tercer lugar: "+ podio.getSegundoLugar().getVecesTercerLugar());
						System.out.println("Tercer lugar: "+ podio.getTercerLugar().getApellidos() +" " +podio.getTercerLugar().getNombres());
						System.out.println("veces primer lugar: "+ podio.getTercerLugar().getVecesPrimerLugar());
						System.out.println("veces segundo lugar: "+ podio.getTercerLugar().getVecesSegundoLugar());
						System.out.println("veces tercer lugar: "+ podio.getTercerLugar().getVecesTercerLugar());
						}
				}
			}
		}
		
	}
}
