package sofka;

import org.springframework.beans.factory.annotation.Autowired;

import sofka.business.PistaService;
import sofka.entities.Pista;

public class JuegoPrincipal {

	@Autowired
	PistaService pistaService;
	
	
	public Pista CrearPista() {
		Pista pista = new Pista();
		pista.setDistanciaLimite(null);
		pista.setNombre(null);
		return pistaService.crearPista(pista);
	}
	
}
