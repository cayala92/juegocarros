package sofka.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "carros")
public class Carro {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	String nombre;
	String modelo;
	String color;
	String placa;
	Long distancia;
	@OneToOne(mappedBy = "carro", cascade = CascadeType.ALL)	
    private Conductor conductor;
	@OneToOne(mappedBy = "carro", cascade = CascadeType.ALL)	
    private Carril carril;
}
