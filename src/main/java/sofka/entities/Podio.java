package sofka.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "podios")
public class Podio {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	@OneToOne
    @JoinColumn(name = "FK_JUGADOR1ERLUGAR", updatable = false, nullable = false)
	Jugador primerLugar;
	@OneToOne
    @JoinColumn(name = "FK_JUGADOR2DOLUGAR", updatable = false, nullable = false)
	Jugador segundoLugar;
	@OneToOne
    @JoinColumn(name = "FK_JUGADOR3ERLUGAR", updatable = false, nullable = false)
	Jugador tercerLugar;
	
	@OneToOne(mappedBy = "podio", cascade = CascadeType.ALL)
    private Juego juego;
}
