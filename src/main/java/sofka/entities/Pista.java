package sofka.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "pistas")
public class Pista {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	String nombre;
	Long distanciaLimite;
	@OneToOne(mappedBy = "pista", cascade = CascadeType.ALL)
    private Juego juego;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "pista")
    private List<Carril> carriles;
	
	
}
