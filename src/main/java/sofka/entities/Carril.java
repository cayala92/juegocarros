package sofka.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "carriles")
public class Carril {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	String nombre;

	@ManyToOne
    @JoinColumn(name = "FK_PISTA", nullable = false, updatable = false)
    private Pista pista;
	
	@OneToOne
	@JoinColumn(name = "FK_CARRO1", updatable = false, nullable = false)
	private Carro carro;
	
}
