package sofka.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "conductores")
public class Conductor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	String apodo;
	@OneToOne	
    @JoinColumn(name = "FK_CARRO", updatable = false, nullable = false)
	private Carro carro;
	@OneToOne(mappedBy = "conductor", cascade = CascadeType.ALL)
    private Jugador jugador;
	
}
