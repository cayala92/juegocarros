package sofka.entities;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "jugadores")
public class Jugador {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;
	String nombres;
	String apellidos;
	int edad;
	String tipoDocumento;
	String numDocumento;
	Long vecesPrimerLugar;
	Long vecesSegundoLugar;
	Long vecesTercerLugar;
	@OneToOne
    @JoinColumn(name = "FK_CONDUCTOR", updatable = false, nullable = false)	
	private Conductor conductor;
	@ManyToOne
    @JoinColumn(name = "FK_JUEGO", nullable = false, updatable = false)
    private Juego juego;
	@OneToOne(mappedBy = "primerLugar", cascade = CascadeType.ALL)
	private Podio podio;
	@OneToOne(mappedBy = "segundoLugar", cascade = CascadeType.ALL)
	private Podio podio2;
	@OneToOne(mappedBy = "tercerLugar", cascade = CascadeType.ALL)
	private Podio podio3;
	
}
