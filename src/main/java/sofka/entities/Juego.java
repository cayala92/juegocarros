package sofka.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "juegos")
public class Juego {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;	
	@OneToOne
	@JoinColumn(name = "FK_PISTA", updatable = false, nullable = false)
	private Pista pista;	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "juego")
	List<Jugador> jugadores;
	@OneToOne
	@JoinColumn(name = "FK_PODIO", updatable = false, nullable = false)
	private Podio podio;
	
}
